package fr.vux.loupMC.main;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;

public enum Config {

    CONFIG("config.yml");



    private final String mainPath = "./plugins/LoupMC/";
    private final String ressourcesPath = "/fr/vux/loupMC/ressouces/";

    public String name;
    public String path;

    public HashMap<String, String> messages = new HashMap<>();

    Config(String name){
        this.name = name;
        this.path = mainPath + name;
    }

    public static void checkConfigs(){
        for(int i = 0; i < Config.values().length; i++){
            File file = new File(Config.values()[i].path);

            if(!file.exists()){
                Config.values()[i].createConfig();
            }
        }
    }

    public void createConfig(){
        try{
            new File(this.path).getParentFile().mkdir();

            InputStream sourceFile = this.getClass().getResourceAsStream(ressourcesPath + this.name);
            FileOutputStream destFile = new FileOutputStream(new File(this.path));

            byte buffer[] = new byte[512 * 1024];
            int nbLecture;

            while((nbLecture = sourceFile.read(buffer)) != -1){
                destFile.write(buffer, 0, nbLecture);
            }

            sourceFile.close();
            destFile.close();

            Bukkit.getLogger().info(this.name + " was created");
        }catch (Exception e){
            e.printStackTrace();
            Bukkit.getLogger().warning("Fail to create file " + this.name);
        }
    }

    @NotNull
    public YamlConfiguration getConfig(){
        return YamlConfiguration.loadConfiguration(new File(this.path));
    }

    public void setConfig(YamlConfiguration config){
        try {
            config.save(new File(this.path));
        } catch (Exception e) {
            e.printStackTrace();
            Bukkit.getLogger().warning("Fail to save " + this.name);
        }
    }

    public void setElement(String path, Object value){
        YamlConfiguration config = this.getConfig();
        config.set(path, value);
        this.setConfig(config);
    }

}
