package fr.vux.loupMC.game;

import fr.vux.loupMC.main.Main;
import fr.vux.loupMC.main.Config;
import fr.vux.loupMC.main.ScoreboardSign;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

public class Party {

    public static Party currentParty;

    public Map<Player, ScoreboardSign> playerScoreboard;
    private String state;
    private int time = Config.CONFIG.getConfig().getInt("time") * 60;
    private int schedulerId;
    private Player wolf;

    public Party(){
        this.playerScoreboard = new HashMap<>();
        this.state = "stop";
        currentParty = this;
    }

    public void init(){
        for(Player player: Bukkit.getOnlinePlayers()){
            ScoreboardSign scoreboard = new ScoreboardSign(player, "LoupMC");
            this.playerScoreboard.put(player, scoreboard);
            scoreboard.create();

            scoreboard.setObjectiveName("LoupMC");
            scoreboard.setLine(0, "Nb Joueurs : ");
            scoreboard.setLine(1, Bukkit.getOnlinePlayers().size() +"");
            scoreboard.setLine(2, "§e ");
            scoreboard.setLine(3, "Loup : ");
            scoreboard.setLine(4, "--------");
            scoreboard.setLine(5, "§2 ");
            scoreboard.setLine(6, "Temps restant : ");
            scoreboard.setLine(7, (this.time/60) + " : 00");

            player.sendMessage("[LoupMC] La partie va commencer d'ici peux de temps");
        }
        this.state = "init";
    }

    public void start(){
        this.state = "start";
        this.selectNewWolf();
        for(Player player : this.playerScoreboard.keySet()){
            player.sendMessage("[LoupMC] La partie commence");
            player.sendMessage("[LoupMC] " + this.wolf.getName() + " est le premier loup");
        }

        this.schedulerId = Bukkit.getScheduler().scheduleAsyncRepeatingTask(Main.plugin, new Runnable() {
            @Override
            public void run() {
                Party.currentParty.time --;
                Party.currentParty.updateScoreboardTimer();

                if(Party.currentParty.time == 0){
                    Party.currentParty.stop();
                }
            }
        }, 0, 20);
    }

    public void stop(){
        this.state = "stop";
        Bukkit.getScheduler().cancelTask(this.schedulerId);

        for(Player player : this.playerScoreboard.keySet()){
            player.sendMessage("[LoupMC] La partie est termine");
            player.sendMessage("[LoupMC] " + this.wolf.getName() + " fut le dernier loup");
        }

        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
            @Override
            public void run() {
               for(Player player : Party.currentParty.playerScoreboard.keySet()){
                   Party.currentParty.playerScoreboard.get(player).destroy();
               }
               new Party();
            }
        }, 5);
    }

    public String getState(){
        return(this.state);
    }

    private void updateScoreboardTimer(){
        if(this.time%60 < 10){
            for(Player player : this.playerScoreboard.keySet()){
                this.playerScoreboard.get(player).setLine(7, (this.time/60) + " : 0" + (this.time%60));
            }
        }else{
            for(Player player : this.playerScoreboard.keySet()){
                this.playerScoreboard.get(player).setLine(7, (this.time/60) + " : " + (this.time%60));
            }
        }
    }

    private void updateScoreboardWolf(){
        for(Player player : this.playerScoreboard.keySet()){
            this.playerScoreboard.get(player).setLine(4, this.wolf.getName());
        }
    }

    public void updateScoreboardNumberPlayers(){
        Bukkit.getScheduler().scheduleAsyncDelayedTask(Main.plugin, new Runnable() {
            @Override
            public void run() {
                for(Player player : Party.currentParty.playerScoreboard.keySet()){
                    Party.currentParty.playerScoreboard.get(player).setLine(1, Bukkit.getOnlinePlayers().size()+"");
                }
            }
        }, 20);
    }

    public boolean isWolf(Player player){
        return(this.wolf.equals(player));
    }

    public void setWolf(Player newWolf){
        this.wolf = newWolf;
        this.updateScoreboardWolf();
    }

    public void selectNewWolf(){
        int rand = new Random().nextInt(this.playerScoreboard.size()) -1;
        Iterator<Player> playerList = this.playerScoreboard.keySet().iterator();
        for(int i = 0; i<rand; i++){
            playerList.next();
        }

        this.setWolf(playerList.next());
    }

    public String getWolfName(){
        return(this.wolf.getName());
    }

}
