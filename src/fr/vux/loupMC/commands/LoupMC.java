package fr.vux.loupMC.commands;

import fr.vux.loupMC.game.Party;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class LoupMC implements CommandExecutor{

    private CommandSender sender;
    private String cmd;
    private String[] args;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {

        this.sender = sender;
        this.cmd = cmd;
        this.args = args;

        if(args[0].equalsIgnoreCase("init")){
            this.init();
        }else if(args[0].equalsIgnoreCase("start")){
            this.start();
        }else if(args[0].equalsIgnoreCase("stop")){
            this.stop();
        }else{
            sender.sendMessage("[LoupMC] Cette commande n'existe pas");
        }
        return(true);
    }

    private void init(){
        if(Party.currentParty.getState().equals("stop")){
            Party.currentParty.init();
        }else{
            this.sendErrorMessage("La partie ne peux pas etre initialiser");
        }
    }

    private void start(){
        if(Party.currentParty.getState().equals("init")){
            Party.currentParty.start();
        }else{
            this.sendErrorMessage("La partie ne peux pas encore commencer");
        }
    }

    private void stop(){
        if(Party.currentParty.getState().equals("start")){
            Party.currentParty.stop();
        }else{
            this.sendErrorMessage("La partie ne peux pas etre arreter");
        }
    }

    private void sendErrorMessage(String msg){
        String prefix = "[LoupMC] ";
        this.sender.sendMessage(prefix + msg);
    }

}
