**Loup MC**

Plugin Minecraft 1.11.2
Reprend le principe du loup touche-touche


_Commandes_ : 

    /loupMC init
        Permet d'initialiser la partie avec les joueurs connecté sur le serveur

    /loupMC start
        Permet de lancer la partie quand elle est initialisée

    /loupMC stop
        Permet de stopper la partie avant la fin de la partie